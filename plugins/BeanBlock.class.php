<?php
/**
 * @file
 * Listing bean plugin.
 */

class BeanBlock extends BeanPlugin {
  /**
   * Declares default block settings.
   */
  public function values() {
    $values = array(
      'settings' => array(
        'block' => '',
      ),
    );

    return array_merge(parent::values(), $values);
  }
  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {
    $form = array();

    $form['settings'] = array(
      '#type' => 'fieldset',
      '#tree' => 1,
      '#title' => t('Choose a block'),
    );

    $form['settings']['module'] = array(
      '#type' => 'textfield',
      '#title' => t('Module'),
      '#default_value' => isset($bean->settings['module']) ? $bean->settings['module'] : '',
      '#required' => TRUE,
    );

    $form['settings']['delta'] = array(
      '#type' => 'textfield',
      '#title' => t('Delta'),
      '#default_value' => isset($bean->settings['delta']) ? $bean->settings['delta'] : '',
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
//    dpm($bean);

    $block = module_invoke($bean->settings['module'], 'block_view', $bean->settings['delta']);

    $render = array(
      '#markup' =>  $block['content'],
    );

    return $render;
  }
}
